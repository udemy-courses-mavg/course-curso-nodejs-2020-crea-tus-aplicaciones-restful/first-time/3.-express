const express = require('express');
const app = express();
const car = require('./routes/car');
app.use(express.json());
app.use('/api/cars/', car);
// Utliza la variable de entorno que nos asigna el provedor (por ejemplo un servido de AWS) o utiliza el puerto 3003
const port = process.env.port || 3003
app.listen(port, ()=> console.log("Escuchando en el puerto " + port + "..."));

/** ============================================================================= 
 * ================================= Middleware =================================
 * ============================================================================== */

/*const date = require('./date');
const morgan = require('morgan');

app.use(morgan('tiny'));

app.use(date);
app.use(function (req, res, next){
    console.log('Time: ', Date.now());
    next();
})

app.use('/api/cars/list', function(req, res, next){
    console.log('Request Type: ', req.method);
    next();
})
*/
